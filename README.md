 		!Bienvenido a Nuestro Proyecto de Aplicación Movil¡
		
		
		Requisitos para correr la aplicacion 
		
		-Tener instalado previamente PHP ver 7.2.33 en su maquina
		-Tener instalado pgAdmin version 4.5 como motor de base de datos para postgresSQL version
		-Tener Node Js version 14.5.3 y NPM version 6.14.8
		
		Instrucciones 
		-Clonar el repositorio con URL de tipo SSH git@gitlab.com:Janoscar/test-programador.git
		-Una vez clonado el repositorio, inciar el servirdor backend desde el directorio del repositorio test-programador\backend-laravel\ Ejecutando el comando php artisan serve en la terminal
		-Levantar el servicio de postgresSQL
		-Iniciar el frontend desde el directorio \test-programador\frontend-react-js ejecutando el comando npm start en la terminal
		
		Api Rest para consumir la data
		
		http://127.0.0.1:8000/regiones 
		http://127.0.0.1:8000/provincias
		http://127.0.0.1:8000/ciudades
		http://127.0.0.1:8000/calles