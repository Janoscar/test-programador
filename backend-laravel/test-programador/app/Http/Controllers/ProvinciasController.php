<?php

namespace App\Http\Controllers;

use App\provincias;
use App\regiones;
use Illuminate\Http\Request;

class ProvinciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\provincias  $provincias
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->id_reg == null && $request->id_reg == "") {
            return provincias::all();
        } else {
            return provincias::where('id_reg', $request->id_reg)->get();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\provincias  $provincias
     * @return \Illuminate\Http\Response
     */
    public function edit(provincias $provincias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\provincias  $provincias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, provincias $provincias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\provincias  $provincias
     * @return \Illuminate\Http\Response
     */
    public function destroy(provincias $provincias)
    {
        //
    }
}
