<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincias', function (Blueprint $table) {
            $table->id('id_pro');
            /*creamos el id de la region que por defecto es de tipo bigInteger*/
            $table->bigInteger('id_reg')->unsigned();
            /*creamos la referencia a la tabla */
            $table->foreign('id_reg')->references('id_reg')->on('regiones');
            $table->timestamps();
            $table->string('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provincias');
    }
}
