<?php

namespace App\Http\Controllers;

use App\ciudades;
use Illuminate\Http\Request;

class CiudadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->id_pro == null && $request->id_pro == "") {
            return ciudades::all();
        } else {
            return ciudades::where('id_pro', $request->id_pro)->get();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function edit(ciudades $ciudades)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ciudades $ciudades)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ciudades  $ciudades
     * @return \Illuminate\Http\Response
     */
    public function destroy(ciudades $ciudades)
    {
        //
    }
}
