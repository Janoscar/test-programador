import React, { Component, useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios';
import { element } from 'prop-types';


/* Creando combox como componente para ser llamado en la clase principal*/
class Combobox_region extends Component {
  render() {
    return (
      <div>
        <select id="region">
          <option selected>Seleccione Region</option>
          <option id={this.props.id_reg} value={this.props.value_reg}>{this.props.nombre_region}</option>
        </select>
      </div>
    )
  }
}

class Combobox_provincia extends Component {
  render() {
    return (
      <div>
        <select id="provincia">
          <option selected>Seleccione provincia</option>
          <option>{this.props.nombre_provincia}</option>
        </select>
      </div>
    )
  }
}

class Combobox_ciudad extends Component {
  render() {
    return (
      <div>
        <select id="ciudad">
          <option selected>Seleccione ciudad</option>
          <option>{this.props.nombre_ciudad}</option>
        </select>
      </div>
    )
  }
}

class Grilla extends Component {
  render() {
    return (
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre Calles</th>
            <th scope="col">Numero</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">{this.props.identificador}</th>
            <td>{this.props.nombre_calle}</td>
            <td>{this.props.numero_calle}</td>
          </tr>
        </tbody>
      </table>
    )
  }
}

class App extends Component {

  state = {
    regiones: [],
    provincias: [],
    ciudades: [],
    calles: []
  }

  componentDidMount() {
    axios.get("http://127.0.0.1:8000/regiones").
      then(response => {
        console.log(response);
        this.setState({ regiones: response.data })
      }).
      catch((error) => {
        console.log(error);
      });

    axios.get("http://127.0.0.1:8000/provincias").
      then(response => {
        console.log(response)
        this.setState({ provincias: response.data })
      }).
      catch((error) => {
        console.log(error);
      });

    axios.get("http://127.0.0.1:8000/ciudades").
      then(response => {
        console.log(response);
        this.setState({ ciudades: response.data })
      }).
      catch((error) => {
        console.log(error);
      });

    axios.get("http://127.0.0.1:8000/calles").
      then(response => {
        console.log(response);
        this.setState({ calles: response.data })
      }).
      catch((error) => {
        console.log(error);
      });
  }



  render() {
    return (
      <div className="principal">
        <h1 className="titulo">Test Programador</h1>
        <div className="combobox">
          <select name="seleccione region">
            {this.state.regiones.map(elemento => (
              <option key={elemento.id_reg} value={elemento.id_reg}>{elemento.nombre}</option>
            ))}
          </select>
          <select name="seleccione region">
            {this.state.provincias.map(elemento => (
              <option key={elemento.id_pro} value={elemento.id_pro}>{elemento.nombre}</option>
            ))}
          </select>
          <select name="seleccione region">
            {this.state.ciudades.map(elemento => (
              <option key={elemento.id_ciu} value={elemento.id_ciu}>{elemento.nombre}</option>
            ))}
          </select>
        </div>

        <div className="tabla">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre Calles</th>
              </tr>
            </thead>
            <tbody>
              {this.state.calles.map(elemento => (
                <tr>
                  <th scope="row">{elemento.id_ciu}</th>
                  <td>{elemento.nombre}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default App;
