<?php

use App\calles;
use App\ciudades;
use App\provincias;
use App\regiones;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        /*creacion de regiones */
        $region = new regiones();
        $region->nombre = "Bío-Bío";
        $region->save();

        $region = new regiones();
        $region->nombre = "Araucanía";
        $region->save();

        /*Provincias de Region del Bío-Bío*/
        $provincia = new provincias();
        $provincia->nombre = "Arauco";
        $provincia->id_reg = "1";
        $provincia->save();

        $provincia = new provincias();
        $provincia->nombre = "Bío-Bío";
        $provincia->id_reg = "1";
        $provincia->save();

        $provincia = new provincias();
        $provincia->nombre = "Concepción";
        $provincia->id_reg = "1";
        $provincia->save();

        /*Provincias de Region de la Araucania*/
        $provincia = new provincias();
        $provincia->nombre = "Cautín ";
        $provincia->id_reg = "2";
        $provincia->save();

        $provincia = new provincias();
        $provincia->nombre = "Malleco";
        $provincia->id_reg = "2";
        $provincia->save();
/*Bio-Bio*/
        /*Ciudades de la la provincias de Arauco*/
        $ciudad = new ciudades();
        $ciudad->nombre="Lebu";
        $ciudad->id_pro="1";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Cañete";
        $ciudad->id_pro="1";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Carampangue";
        $ciudad->id_pro="1";
        $ciudad->save();

        /*Ciudades de la la provincias de Bío-Bío*/
        $ciudad = new ciudades();
        $ciudad->nombre="Laja";
        $ciudad->id_pro="2";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Cabrero";
        $ciudad->id_pro="2";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Los Angeles";
        $ciudad->id_pro="2";
        $ciudad->save();

         /*Ciudades de la la provincias de Concepción*/
         $ciudad = new ciudades();
         $ciudad->nombre="Coronel";
         $ciudad->id_pro="3";
         $ciudad->save();
 
         $ciudad = new ciudades();
         $ciudad->nombre="Penco";
         $ciudad->id_pro="3";
         $ciudad->save();
 
         $ciudad = new ciudades();
         $ciudad->nombre="Tome";
         $ciudad->id_pro="3";
         $ciudad->save();

/*Araucania*/
        /*Ciudades de la la provincias de Cautin*/
        $ciudad = new ciudades();
        $ciudad->nombre="Temuco";
        $ciudad->id_pro="4";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Pucon";
        $ciudad->id_pro="4";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Villarica";
        $ciudad->id_pro="4";
        $ciudad->save();

        /*Ciudades de la la provincias de Malleco*/
        $ciudad = new ciudades();
        $ciudad->nombre="Angol";
        $ciudad->id_pro="5";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Collipulli";
        $ciudad->id_pro="5";
        $ciudad->save();

        $ciudad = new ciudades();
        $ciudad->nombre="Purén";
        $ciudad->id_pro="5";
        $ciudad->save();

/*calles*/
        $calle = new calles();
        $calle->id_ciu="1";
        $calle->nombre="Freire";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="1";
        $calle->nombre="Lillo";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="2";
        $calle->nombre="Uribe";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="2";
        $calle->nombre="Tucapel";
        $calle->save();
        
        $calle = new calles();
        $calle->id_ciu="3";
        $calle->nombre="Monsalve";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="3";
        $calle->nombre="Republica";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="4";
        $calle->nombre="Genova";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="4";
        $calle->nombre="Tapiacura";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="5";
        $calle->nombre="Las Perlas";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="5";
        $calle->nombre="Florida";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="6";
        $calle->nombre="Balmaceda";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="6";
        $calle->nombre="Alemania";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="7";
        $calle->nombre="Julio Martínez Cifuentes";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="7";
        $calle->nombre="Minero Gabriel Gana";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="8";
        $calle->nombre="Yerbas Buenas";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="8";
        $calle->nombre="Alcazar";
        $calle->save();
        
        $calle = new calles();
        $calle->id_ciu="9";
        $calle->nombre="Ercilla";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="9";
        $calle->nombre="Dagnino";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="10";
        $calle->nombre="Holandesa";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="10";
        $calle->nombre="San Ernesto";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="11";
        $calle->nombre="Brasil";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="11";
        $calle->nombre="Urugay";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="12";
        $calle->nombre="Gran Kornel";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="12";
        $calle->nombre="Julio Zegers";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="13";
        $calle->nombre="Las Rosas";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="13";
        $calle->nombre="Maipu";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="14";
        $calle->nombre="Carrera";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="14";
        $calle->nombre="Caupolican";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="15";
        $calle->nombre="Villagra";
        $calle->save();

        $calle = new calles();
        $calle->id_ciu="15";
        $calle->nombre="Imperial";
        $calle->save();
    }
}