<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegionesController;
use App\Http\Controllers\ProvinciasController;
use App\Http\Controllers\CiudadesController;
use App\Http\Controllers\CallesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    /*return view('welcome');*/

    return "Bienvenidos a laravel";
});

/* ruta para mostrar las regiones y llamando al metodo show*/
Route::get('regiones', [RegionesController::class, 'show']);
/* ruta para mostrar las provincias y llamando al metodo show, pasando variable por la ruta*/
Route::get('provincias/{id_reg?}', [ProvinciasController::class, 'show', $id_reg = ""]);
/* ruta para mostrar las ciudades y llamando al metodo show, pasando variable por la ruta*/
Route::get('ciudades/{id_pro?}', [CiudadesController::class , 'show', $id_pro = ""]);
/* ruta para mostrar las calles y llamando al metodo show, pasando variable por la ruta*/
Route::get('calles/{id_ciu?}', [CallesController::class , 'show', $id_ciu = ""]);